package hello

import "testing"

func Test_helloWorld(t *testing.T) {
	helloWorld := helloWorld()
	esperado := "Hello World!"

	if helloWorld != esperado {
		t.Fatalf("mensage diferente de 'Hello World!'")
	}
}
