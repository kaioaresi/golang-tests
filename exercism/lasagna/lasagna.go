package lasagna

const OvenTime float64 = 40

func RemainingOvenTime(actual int) int {
	return actual - int(OvenTime)
}

func PreparationTime(numberOfLayers int) int {
	return numberOfLayers * 2
}
