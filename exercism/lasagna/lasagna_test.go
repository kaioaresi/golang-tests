package lasagna

import (
	"testing"
)

func Test_RemainingOvenTime(t *testing.T) {
	esperado := 30

	if RemainingOvenTime(10) != esperado {
		t.Errorf("RemainingOvenTime() deveria retorna %v", esperado)
	}
}
