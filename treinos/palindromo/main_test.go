package main

import "testing"

func Test_invertText(t *testing.T) {
	type Caso struct {
		text     string
		esperado string
	}

	textLists := []Caso{
		{
			text:     "ana",
			esperado: "ana",
		},
		{
			text:     "ovo",
			esperado: "ovo",
		},
		{
			text:     "zed",
			esperado: "dez",
		},
	}

	for _, textList := range textLists {
		if invertText(textList.text) != textList.esperado {
			t.Errorf("Falou: %v", textList)
		}
	}
}

func TestIsPalindromo(t *testing.T) {
	type Caso struct {
		text     string
		esperado bool
	}

	testCasos := []Caso{
		{
			text:     "ovo",
			esperado: true,
		},
		{
			text:     "ana",
			esperado: true,
		},
		{
			text:     "Zed",
			esperado: false,
		},
		{
			text:     "",
			esperado: false,
		},
		{
			text:     "arara",
			esperado: true,
		},
	}

	for _, testCaso := range testCasos {
		if isPalindromo(testCaso.text) != testCaso.esperado {
			t.Errorf("Falou: %v", testCaso)
		}
	}
}

func TestIsPalindromoB(t *testing.T) {
	type Caso struct {
		text     string
		esperado bool
	}

	testCasos := []Caso{
		{
			text:     "ovo",
			esperado: true,
		},
		{
			text:     "ana",
			esperado: true,
		},
		{
			text:     "Zed",
			esperado: false,
		},
		{
			text:     "",
			esperado: false,
		},
		{
			text:     "arara",
			esperado: true,
		},
	}

	for _, testCaso := range testCasos {
		if isPalindromoB(testCaso.text) != testCaso.esperado {
			t.Errorf("Falou: %v", testCaso)
		}
	}
}

func BenchmarkIsPalindromo(b *testing.B) {
	for i := 0; i <= b.N; i++ {
		isPalindromo("arara")
	}
}

func BenchmarkIsPalindromoB(b *testing.B) {
	for i := 0; i <= b.N; i++ {
		isPalindromoB("arara")
	}
}
