package main

// invertText - retorna uma string invertida
func invertText(text string) string {
	var stringInvertida string

	for i := len(text) - 1; i >= 0; i-- {
		stringInvertida += string(text[i])
	}
	return stringInvertida
}

// isPalindromo - verifica se um texto é um palindromo
func isPalindromo(s string) bool {
	if len(s) == 0 {
		return false
	}
	return invertText(s) == s
}

// isPalindromoB - verifica se um texto é um palindromo porém com mais performance
func isPalindromoB(s string) bool {
	if len(s) == 0 {
		return false
	}
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		if s[i] != s[j] {
			return false
		}
	}

	return true
}

func main() {

	invertText("zed")
	isPalindromo("ana")
}
