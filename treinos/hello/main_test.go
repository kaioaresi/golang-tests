package main

import (
	"testing"
)

func Test_hello(t *testing.T) {
	type msg struct {
		nome     string
		esperado string
	}

	cases := []msg{
		{nome: "Lux", esperado: "Hello, Lux"},
		{nome: "", esperado: ""},
	}

	for _, msg := range cases {
		if hello(msg.nome) != msg.esperado {
			t.Errorf("Falou: %+v", msg)
		}
	}
}

func Benchmark_hello(b *testing.B) {
	for i := 0; i <= b.N; i++ {
		hello("Zed")
	}
}
