package main

import "fmt"

func hello(s string) string {
	if len(s) == 0 {
		return ""
	}
	return fmt.Sprintf("Hello, %s", s)
}

func hello2(s string) string {
	if len(s) == 0 {
		return ""
	}
	return fmt.Sprintf("Hello, %s", s)
}

func main() {
	hello("Zed")
	hello2("Shen")
}
