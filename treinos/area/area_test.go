package area

import "testing"

func TestPerimetro(t *testing.T) {
	retangulo := Retangulo{10.0, 10.0}
	resultado := perimetro(retangulo)
	esperado := 40.0

	if resultado != esperado {
		t.Errorf("Resultado: %0.2f, esperado %0.2f", resultado, esperado)
	}
}

func TestArea(t *testing.T) {
	retangulo := Retangulo{12.0, 6.0}
	resultado := retangulo.area() // chamando metodo area para retangulo
	esperado := 72.0

	if resultado != esperado {
		t.Errorf("Resultado: %0.2f, esperado: %0.2f", resultado, esperado)
	}
}

func TestCirculo(t *testing.T) {
	circulo := Circulo{10}
	resultado := circulo.area() // chamado metodo para circulo
	esperado := 314.1592653589793

	if resultado != esperado {
		t.Errorf("Resltado: %0.2f, esperado: %0.2f", resultado, esperado)
	}
}

func TestArea2(t *testing.T) {
	verificaArea := func(t *testing.T, forma Forma, esperado float64) {
		t.Helper()
		resultado := forma.area()

		if resultado != esperado {
			t.Errorf("Resultado: %0.2f, esperado: %0.2f", resultado, esperado)
		}

	}

	t.Run("retangulos", func(t *testing.T) {
		retangulo := Retangulo{12.0, 6.0}
		verificaArea(t, retangulo, 72.0)
	})
	t.Run("circulo", func(t *testing.T) {
		circulo := Circulo{10}
		verificaArea(t, circulo, 314.1592653589793)
	})
}
