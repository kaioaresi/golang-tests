package area

import "math"

type Retangulo struct {
	Largura float64
	Altura  float64
}

type Circulo struct {
	Raio float64
}

// Passando qual a assinatura de uma forma, para que possamos usar area para retangulos e circulos
type Forma interface {
	area() float64
}

func perimetro(r Retangulo) float64 {
	return 2 * (r.Largura + r.Altura)
}

func (r Retangulo) area() float64 {
	return r.Largura * r.Altura
}

func (c Circulo) area() float64 {
	return math.Pi * c.Raio * c.Raio
}
